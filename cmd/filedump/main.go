package main

/*

Writes file as hex data into a text file.

go run main.go NAMEOFFILE.XXX bytes

"bytes" is an optional amount in case you only need to read
an arbitrary number of bytes from a file

*/

import (
	"encoding/hex"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
)

func main() {
	output := flag.String("o", "hex_dumped.txt", "path to output text file")
	path := flag.String("path", "", "path to input file")
	byteSize := flag.String("bytes", "", "optional number of bytes to read")
	flag.Parse()

	if len(os.Args) <= 1 {
		fmt.Fprintln(os.Stderr, "usage: hex filename [bytes]")
		return
	}

	HexDump(*path, *output, *byteSize)
}

func HexDump(inputPath string, outputFileName string, byteSize string) {
	data, err := ioutil.ReadFile(inputPath)
	if err != nil {
		fmt.Fprintln(os.Stderr, "filename:", err)
		return
	}
	n := len(data)
	if byteSize != "" {
		i, err := strconv.Atoi(byteSize)
		if err != nil {
			fmt.Fprintln(os.Stderr, "bytes:", err)
			return
		}
		if n > i {
			n = i
		}
	}

	//create output file
	outputFile := filepath.FromSlash(outputFileName)

	w, err := os.Create(outputFile)
	if err != nil {
		panic(err)
	}
	defer w.Close()

	_, err = w.Write([]byte(hex.Dump(data[:n])))

	if err != nil {
		fmt.Fprintln(os.Stderr, "hex err: ", err)
	}
}
