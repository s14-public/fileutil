package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/udhos/equalfile"
	"gitlab.com/s14-public/fileutil"
)

func main() {
	//get command line arguments
	if len(os.Args) < 2 {
		fmt.Printf("usage: first directory second directory\n")
		os.Exit(2)
	}

	//get directories
	directory1 := os.Args[1]
	directory2 := os.Args[2]

	//compare directories and write to log file
	err := CompareDirectories("compare-list.txt", directory1, directory2, "", false)
	if err != nil {
		fmt.Printf("couldn't compare directories %s \n", err)
	}
}

//this function gets a list of files from two directories, and compares them for matches
//anything that matches a filename, but has different sizes or date modified is logged
func CompareDirectories(logname string, directory1 string, directory2 string, ext string, recursive bool) error {
	var mismatches []fileutil.Mismatch
	var files1 []fileutil.Record
	var files2 []fileutil.Record

	files1, err := fileutil.GetFiles(directory1, ext, recursive, files1)
	if err != nil {
		return err
	}

	files2, err = fileutil.GetFiles(directory2, ext, recursive, files2)
	if err != nil {
		return err
	}

	//compare all files in directory1 with those in directory2
	for _, file1 := range files1 {
		file2, fileExists := fileutil.SearchForFile(file1.Name, files2)

		if fileExists == true {
			equals, mismatch := compareFiles(file1, file2)
			//if we found a mismatch
			if equals == false {
				mismatches = append(mismatches, mismatch)
			}
		}
	}

	return nil
}

//this function compares file size and time modified to be used when
//comparing directories
func compareFiles(file1 fileutil.Record, file2 fileutil.Record) (bool, fileutil.Mismatch) {
	var mismatch fileutil.Mismatch
	var cmp *equalfile.Cmp
	options := equalfile.Options{}
	cmp = equalfile.New(nil, options)

	equal, err := cmp.CompareFile(file1.Path, file2.Path)
	if err != nil {
		log.Println(err)
	}

	//log details of mismatch
	if equal == false {
		mismatch.Name = file1.Name
		mismatch.Size1 = file1.Size
		mismatch.Date1 = file1.Time
		mismatch.Size2 = file2.Size
		mismatch.Date2 = file2.Time

		return false, mismatch
	}

	return true, mismatch
}

func generateErrorLog(logname string, mismatches []fileutil.Mismatch) {
	file, err := os.Create(logname)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	w := bufio.NewWriter(file)

	for _, mismatch := range mismatches {
		fmt.Fprintln(w, "+--- "+mismatch.Name+" ---+------------------------------------------ \r\n")
		fmt.Fprintln(w, "+--- File 1 size: "+fmt.Sprintf("%v", mismatch.Size1)+" \r\n")
		fmt.Fprintln(w, "+--- File 1 modified: "+mismatch.Date1.Format(time.UnixDate)+" \r\n")
		fmt.Fprintln(w, "+--- File 2 size: "+fmt.Sprintf("%v", mismatch.Size2)+" \r\n")
		fmt.Fprintln(w, "+--- File 2 modified: "+mismatch.Date2.Format(time.UnixDate)+" \r\n")
		fmt.Fprintf(w, "+------------------------------------------------------------------------ \r\n")
		fmt.Fprintf(w, "\r\n")
	}

	w.Flush()
}
