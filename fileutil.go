package fileutil

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"
)

type Mismatch struct {
	Name  string
	Size1 int64
	Size2 int64
	Date1 time.Time
	Date2 time.Time
}

type Record struct {
	Name       string
	Path       string
	Size       int64
	Time       time.Time
	LineNumber int64
}

//get a basic list of all file names in a directory
func GetFileNames(fileDirectory string) ([]string, error) {
	var filenames []string

	directoryData, err := os.Open(filepath.FromSlash(fileDirectory))
	if err != nil {
		return filenames, err
	}
	defer directoryData.Close()

	filenames, err = directoryData.Readdirnames(0)
	if err != nil {
		return filenames, err
	}

	return filenames, nil
}

//compare two arrays of filenames and return the difference
func CompareFileNames(a []string, b []string, ext string) []string {
	results := make([]string, 0, 0)
	var match bool

	for _, r := range a {
		match = false

		for _, rr := range b {
			if r == rr {
				match = true
			}
		}

		fileExt := filepath.Ext(r)

		if match == false && fileExt == ext {
			results = append(results, r)
		}
	}

	return results
}

//Get an in depth file list from a given directory
func GetFiles(dir string, fileExt string, recursive bool, records []Record) ([]Record, error) {
	dirPath := filepath.FromSlash(dir)

	//get array of file information
	fileInfos, err := ioutil.ReadDir(dirPath)
	if err != nil {
		return records, err
	}

	//loop over files
	for _, fileInfo := range fileInfos {
		//see if we need to recursively check directories
		if fileInfo.IsDir() && recursive {
			fp := fmt.Sprintf("%s/%s", dirPath, fileInfo.Name())
			records, err = GetFiles(fp, fileExt, recursive, records)
			if err != nil {
				//log this?
			}
		}

		//if extension isn't blank, only grab those that match
		if fileExt != "" {
			if fileExt == strings.ToLower(path.Ext(fileInfo.Name())) {
				records = SetRecordData(dirPath, records, fileInfo)
			}
		} else {
			records = SetRecordData(dirPath, records, fileInfo)
		}
	}

	return records, nil
}

//this function opens a file and reads line by line into a slice
func OpenFileSlice(filename string) ([]string, error) {
	var fileSlice []string

	file, err := os.Open(filepath.FromSlash(filename))
	if err != nil {
		return fileSlice, err
	}
	defer file.Close()

	//loop over buffer and copy data into slice
	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		fileSlice = append(fileSlice, scanner.Text())
	}

	return fileSlice, nil
}

//populates a slice of type Record with information about files
//in a given directory
func SetRecordData(dirPath string, records []Record, fileInfo os.FileInfo) []Record {
	var filePath string
	var fileName string
	var fileSize int64
	var fileDate time.Time

	filePath = fmt.Sprintf("%s/%s", dirPath, fileInfo.Name())
	fileName = fileInfo.Name()
	fileSize = fileInfo.Size()
	fileDate = fileInfo.ModTime()

	//make sure this isn't a directory
	if fileInfo.IsDir() == false {
		var record Record
		record.Name = fileName
		record.Path = filepath.FromSlash(filePath)
		record.Size = fileSize
		record.Time = fileDate
		records = append(records, record)
	}

	return records
}

//helper function to find a single Record in a list
func SearchForFile(filename string, files []Record) (Record, bool) {
	for _, file := range files {
		if filename == file.Name {
			return file, true
		}
	}

	var empty Record
	return empty, false
}

//copy one file to another destination
func CopyFile(src string, dest string) (int64, error) {
	file, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer file.Close()

	copy, err := os.Create(dest)
	if err != nil {
		return 0, err
	}
	defer copy.Close()

	wrote, err := io.Copy(copy, file)
	if err != nil {
		return 0, err
	}

	return wrote, nil
}

//copy files from one directory to another
func CopyDirectory(src string, dest string) error {
	files, err := ioutil.ReadDir(src)
	if err != nil {
		return err
	}

	//copy each file over
	for _, f := range files {
		//get file name
		srcFile := fmt.Sprintf("%s/%s", src, f.Name())
		destFile := fmt.Sprintf("%s/%s", dest, f.Name())

		_, err := CopyFile(filepath.FromSlash(srcFile), filepath.FromSlash(destFile))
		if err != nil {
			return err
		}
	}

	return nil
}

//this function searches a text file for a keyword.
func SearchFile(filePath string, search string) ([]Record, bool) {
	//open file
	file, err := os.Open(filePath)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	//slice of instances and bool
	var instances []Record
	var anyMatches bool

	scanner := bufio.NewScanner(file)

	//scan for instance of string
	var line int64 = 1
	for scanner.Scan() {
		if strings.Contains(strings.ToLower(scanner.Text()), strings.ToLower(search)) {
			var record Record
			record.Name = filepath.Base(filePath)
			record.Path = filePath
			record.LineNumber = line
			instances = append(instances, record)
			anyMatches = true
		}
		line++
	}

	return instances, anyMatches
}
