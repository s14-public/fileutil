package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"gitlab.com/s14-public/fileutil"
)

/*

Simple program to compare two text lists, and print the differences.
Place [[next]] on a line in between the two lists to signify where
one list ends, and the other begins

*/

type Lists struct {
	MainList    []string
	CompareList []string
}

var seperator string = "[[next]]"

func main() {
	if len(os.Args) < 1 {
		fmt.Println("please specify a text file")
		time.Sleep(2 * time.Second)
		os.Exit(1)
	}

	listPtr := flag.String("list", "", "text file containing two lists two compare")
	flag.Parse()

	listName := *listPtr

	lists := splitLists(listName)
	diffs := compareLists(lists)

	if len(diffs) > 0 {
		for _, r := range diffs {
			fmt.Println(r)
		}
	} else {
		fmt.Println("both lists match")
	}
}

func splitLists(filename string) Lists {
	var lists Lists
	var listBreak bool

	lines, err := fileutil.OpenFileSlice(filename)
	if err != nil {
		log.Fatal(err)
	}

	for _, line := range lines {
		trimmedLine := trimLine(line)
		smallLine := strings.ToLower(trimmedLine) //convert to lowercase

		if listBreak {
			lists.CompareList = append(lists.CompareList, smallLine)
		} else {
			if smallLine == seperator {
				listBreak = true
				continue
			}

			lists.MainList = append(lists.MainList, smallLine)
		}
	}

	return lists
}

func compareLists(lists Lists) []string {
	var diff []string

	for _, main := range lists.MainList {
		var matched bool

		for _, comp := range lists.CompareList {
			if main == comp {
				matched = true
				break
			}
		}

		if !matched {
			diff = append(diff, main)
		}
	}

	return diff
}

//this function trims a line that comes from a dbase
//window listing table info. It starts at the first field name
func trimLine(line string) string {
	var newLine string
	var buf []string
	split := strings.Split(line, "")

	for i, r := range split {
		if i > 6 && i < 41 {
			buf = append(buf, r)
		}
	}

	newLine = strings.TrimRight(strings.Join(buf, ""), " ")

	return newLine
}
