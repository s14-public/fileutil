package main

/*

This package is a utility that generates large(ish) numbers of directories. It's
primary use is to create a directory system for distributing documents.

Since storing a large number of docs in one folder is bad for performance, randomly
storing them in an array of folders spreads the load. This package is a convenient
way to create / delete a large number of folders

Examples:

[standard usage]
dirgen /home/user/files 500 0

This above will create 500 folders in C:\directory, starting at the number 0

*/

import (
	"fmt"
	"os"
	"strconv"
)

func main() {
	//get command line arguments
	if len(os.Args) < 3 {
		fmt.Printf("usage: [directory to create folders in] [number of folders] [starting number]\n")
		os.Exit(2)
	}

	//get cli arguments
	directory := os.Args[1]
	numArg := os.Args[2]
	startArg := os.Args[3]

	folderNum, err := strconv.ParseInt(numArg, 10, 64)
	if err != nil {
		fmt.Printf("invalid folder number")
	}

	startNum, err := strconv.ParseInt(startArg, 10, 64)
	if err != nil {
		fmt.Printf("invalid start number")
	}

	//create folders from startNum to folderNum (ie. 0-100)
	for i := startNum; i <= folderNum; i++ {
		folderName := fmt.Sprintf("%03d", i)
		newPath := fmt.Sprintf("%s/%s", directory, folderName)
		os.MkdirAll(newPath, os.ModePerm)
	}
}
