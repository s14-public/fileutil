package main

import (
	"bufio"
	"flag"
	"fmt"
	"gitlab.com/s14-public/fileutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func main() {
	//get command line arguments
	if len(os.Args) < 3 {
		log.Println("add -r for recursion, add -o for a custom output path")
		log.Fatal("Not enough arguments. Add folder path, followed by file extension(s), and the string to search.")
	}

	//slice to build results into
	var mainList []fileutil.Record

	//get flags
	recursivePtr := flag.Bool("r", false, "recursive folder search")
	outputPtr := flag.String("o", "search_results.txt", "path to results file")
	flag.Parse()

	recursive := *recursivePtr
	output := *outputPtr

	//get cli arguments
	args := flag.Args()
	folderName := args[0]
	fileExt := strings.ToLower(args[1])
	searchString := args[2]

	//split comma separated list of file extensions
	exts := strings.Split(fileExt, ",")

	//perform a search per file extension
	for _, ext := range exts {
		mainList = getMatches(folderName, ext, recursive, searchString, mainList)
	}

	//see if we found any matches
	if len(mainList) > 0 {
		generateResultLog(mainList, output)
	} else {
		log.Println("no matches found")
	}
}

//search files based on file extension for text matches
func getMatches(folder string, fileExt string, recursive bool, searchString string, mainList []fileutil.Record) []fileutil.Record {
	var matchList []fileutil.Record

	//get files
	matchList, err := fileutil.GetFiles(folder, fileExt, recursive, matchList)
	if err != nil {
		log.Println(err)
	}

	//loop through records and look for a match
	for _, file := range matchList {
		records, anyMatch := fileutil.SearchFile(file.Path, searchString)

		if anyMatch {
			mainList = addToMainList(mainList, records)
		}
	}

	return mainList
}

func addToMainList(mainList []fileutil.Record, records []fileutil.Record) []fileutil.Record {
	for _, record := range records {
		mainList = append(mainList, record)
	}

	return mainList
}

//create a text file with the search results
func generateResultLog(records []fileutil.Record, path string) {
	var dirPath string //place to save results

	//check if a directory was specified
	dirPath = filepath.FromSlash(path)

	file, err := os.Create(dirPath)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	w := bufio.NewWriter(file)

	for _, record := range records {
		fmt.Fprintf(w, "- File: %s --- Line: %d\r\n", record.Path, record.LineNumber)
	}

	numRecords := len(records)
	log.Println("Number of matches found: ", numRecords)

	w.Flush()
}
